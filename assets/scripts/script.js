let tasks = [];

function addTask(task){
	tasks.push(task);

	return tasks;
}

addTask("eat breakfast");
addTask("code front end");
addTask("eat lunch");
addTask("code back end");
addTask("eat dinner");
addTask("sleep");
addTask("repeat");

function countTasks(){
	return tasks.length;
}

for(let i = 0;i < tasks.length;i++){
	console.log(tasks[i]);
}

function findTask(taskToFind){
	let filteredTask = tasks.filter(function(task){
		if(task.includes(taskToFind)){
			return task;
		}
	});

	return filteredTask;
}